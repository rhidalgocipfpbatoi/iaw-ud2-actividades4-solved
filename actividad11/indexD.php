<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>IAW-UD2-A10</title>
    <link rel="stylesheet" type="text/css" href="../assets/css/style.css">
</head>
<body>
<h1> Actividad 11 - Codificación ASCII</h1>
<?php

$totalImparells = 0;
for ($i = 1; $i < 100; $i++) {
    if ($i % 2 != 0) {
        $totalImparells ++;
        echo $i.",";
    }
}

echo "<h1>Total Imparells: ".$totalImparells."</h1>";

$totalParells = 0;
for ($i = 1; $i < 100; $i++) {
    if ($i % 2 == 0) {
        $totalParells ++;
        echo $i.",";
    }
}

echo "<h1>Total Parells: ".$totalParells."</h1>";

$totalMultiples5 = 0;
for ($i = 1; $i < 100; $i++) {
    if ($i % 5 == 0) {
        $totalMultiples5 ++;
        echo $i.",";
    }
}

echo "<h1>Total Multiples 5: ".$totalMultiples5."</h1>";

?>
</body>
</html>