<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>IAW-UD2-A10</title>
    <link rel="stylesheet" type="text/css" href="../assets/css/style.css">
</head>
<body>
<h1> Actividad 11 - Codificación ASCII</h1>
<?php

for ($i = 0; $i < 30; $i++) {
    $num = rand(1,200);
    if ($num % 2 == 0) {
        echo "El número es par";
    }else{
        echo "El número es impar";
    }
}

?>
</body>
</html>