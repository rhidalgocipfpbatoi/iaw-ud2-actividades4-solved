<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>IAW-UD2-A10</title>
    <link rel="stylesheet" type="text/css" href="../assets/css/style.css">
</head>
<body>
<h1> Actividad 11 - Codificación ASCII</h1>
<?php

$a = 50;
$b = 23;

$totalPares = 0;
for ($i = $b; $b < $a; $b++){
    if($i % 2 == 0){
        $totalPares++;
    }
}
echo $totalPares;

?>
</body>
</html>